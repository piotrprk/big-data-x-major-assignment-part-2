import java.util.Random;

public class ReservoirSampling {
	// keep sample as an array
	int[] S;
	// keep sample size
	int k;

	
	public ReservoirSampling(int k) {
		this.k = k;
		this.S = new int[k];
	}
	// input data frame [s1.. sn]
	// set sample size k
	// result - sampled data
	public int[] sampleData(int[] inputs, int k) {		
		// output data frame
		int[] result = new int[k];
		// random generator
		Random random = new Random();
		// breakpoints to print sample
		int[] breakpoints = {10, 50, 100, 500};
		// breakpoints index
		int j = 0;
		//take input data one by one
		for (int i=0; i<inputs.length; i++) {
			// while i < k fill the reservoir with data on by one			
			if (i < k) {
				result[i] = inputs[i];
			}			
			// when reservoir if full must use random function to 
			// choose which index will be exchanged with i
			if (i >= k) {
				//generate random number between 0 - i
				int index = random.nextInt(i);
				// replace value if index < k
				// so each item in the result has equal probability k/n
				if (index < k)
					result[index] = inputs[i];
			}
			if (j < breakpoints.length && i == breakpoints[j]) {
				System.out.println("n = " + i);
				this.printS(result);
				j++;
			}
		}
		System.out.println("n = " + inputs.length);
		this.printS(result);
		return result;
	}
	// print sample S
	public void printS(int[] S) {		
		System.out.print("[");		
		for (int i=0; i<S.length; i++) {
			System.out.print( S[i] );
			if (i < S.length - 1)
				System.out.print(", ");
		}
		System.out.print("]\n");
	}
	public void printS() {
		this.printS(this.S);
	}
}
