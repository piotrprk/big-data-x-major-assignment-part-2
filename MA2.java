import java.util.ArrayList;
import java.util.Arrays;

public class MA2 {
	// produce array of integers from 1 to k
	public static int[] intArray(int k) {
		int[] result = new int[k];
		for (int i=0; i<k; i++) {
			result[i] = i+1;
		}
		return result;
	}
	public static void main(String[] args) {
		//
		// PART 1

		// reservoir sampling		
		System.out.println("Part 1. Reservoir sampling");
		System.out.println();
		// sample size k
		int k = 10;
		
		// sample integers 1... 1000
		ReservoirSampling R = new ReservoirSampling(k);
		System.out.println("Sample S of integers 1... n = 1000");
		R.S = R.sampleData(intArray(1000), k);

		System.out.println();
		//
		// PART 2
		//
		// Frequent itemsets
		System.out.println("--------------------------------------------------------");
		System.out.println();
		System.out.println("Part 2. Frequent itemsets");
		System.out.println();		
		// generate baskets

		FrequentItemsets T = new FrequentItemsets();		
		int n_baskets = 150;
		int n_items = 150;
		
		for (int i = 1; i <= n_baskets; i++) {
			// create basket
			Basket B = new Basket();
			// add items to basket
			for (int j = 1; j <= n_items; j++) {
				// item j is in the basket i if i%j == 0
				if ( i % j == 0) {
					B.basket.put(String.valueOf(j), 1);
				}
			}
			// add basket to transactions
			T.transactions.add(B);
		}
		// print transactions
		System.out.println("Baskets:");		
		T.printTransactions();
		// set threshold
		int s = 15;
		// print answer
		System.out.println();
		System.out.println("Support treshold: " + s);		
		
		T.frequentItems(s);
		
		System.out.println("Frequent itemsets:");
		T.printItemsets();
		// set threshold
		s = 5;
		// print answer
		System.out.println();
		System.out.println("Support treshold: " + s);		
		T.frequentItems(s);		
		// in which baskets
		ArrayList<String> items = new ArrayList<>( Arrays.asList("5", "20") );
		ArrayList<Integer> baskets = T.whichBaskets(items);
		System.out.println("baskets with items " + items);
		System.out.println(baskets);
		// print frequent items
		T.printSetsLevel(2);
		//
		// PART 3
		//
		// Data Streams
		System.out.println("--------------------------------------------------------");
		System.out.println();
		System.out.println("Part 3. Data Streams");
		System.out.println();		
		
		// input data stream
		int[] stream = {3,1,4,6,5,9};
		
		// Question 1
		System.out.println("== Question 1 ==");
		// hash function is 2x+1 % 32
		// bit length 5
		int a = 2;
		int b = 1;
		int c = 32;
		int len = 5;
		FlajoletMartin FM = new FlajoletMartin(a, b, c, len);
		int max_T = FM.maxTandPrint(stream);
		System.out.println("Max tail length T: " + max_T + " estimated no of distinct elements 2^T: " + ((int)Math.pow(2, max_T)));
		System.out.println();
		
		// Question 2
		System.out.println("== Question 2 ==");
		// hash function is 3x+7 % 32
		// bit length 5
		a = 3;
		b = 7;

		FM = new FlajoletMartin(a, b, c, len);
		max_T = FM.maxTandPrint(stream);
		System.out.println("Max tail length T: " + max_T + " estimated no of distinct elements 2^T: " + ((int)Math.pow(2, max_T)));
		System.out.println();
		
		// Question 3
		System.out.println("== Question 3 ==");
		// hash function is 4x % 32
		// bit length 5
		a = 4;
		b = 0;

		FM = new FlajoletMartin(a, b, c, len);
		max_T = FM.maxTandPrint(stream);
		System.out.println("Max tail length T: " + max_T + " estimated no of distinct elements 2^T: " + ((int)Math.pow(2, max_T)));
		System.out.println();
		
	}

}
