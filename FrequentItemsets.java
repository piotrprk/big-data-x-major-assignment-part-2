import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;

class Basket {
	// to keep shopping list
	// key = name of the product
	// value = number of product items
	TreeMap<String, Integer> basket;
	
	public Basket() {
		this.basket = new TreeMap<>();
	}
	
	public String toString() {
		String out = new String();
		int i = 0;
		for (String k:this.basket.keySet()) {
			out += k;
			if (i++ < this.basket.size() -1 )
				out += ", ";
		}
		return out;
	}
}

public class FrequentItemsets{
	// keep all transactions in a table
	ArrayList<Basket> transactions;
	// keep itemsets in array of HashSets
	ArrayList< TreeMap<String, ArrayList<Integer> > > itemsets;
	// keep support and confidence values for each set
	HashMap<String, Double> support;
	HashMap<String, Double> confidence;
	
	public FrequentItemsets() {
		this.transactions = new ArrayList<>();
		this.itemsets = new ArrayList<>();
		this.support = new HashMap<>();
		this.confidence = new HashMap<>();		
	}
	public FrequentItemsets(String file_name) {
		this.transactions = new ArrayList<>();
		this.itemsets = new ArrayList<>();
		this.support = new HashMap<>();
		this.confidence = new HashMap<>();		
		this.loadDataRaw(file_name);
	}
	
	// import file structure
	// item_name TAB item_name TAB item_name
	// each basket in a new line
	public void loadDataRaw(String file_name) {
		// get the data
		File f = new File(file_name);

		try (Scanner scanner = new Scanner(f)){	
			while(scanner.hasNextLine()) {
				String[] tokens = scanner.nextLine().split("\n");
				// read line by line
				for(String i:tokens) {
					// split line
					String[] line = i.split("\\t");	// get data by split by TAB
					// add file text line as basket
					this.addBasket(line);
				}	
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex);
		}					
	}
	private void addBasket(String[] items) {
		Basket basket = new Basket();
		for(String s:items) {
			int n = 0;
			if ( basket.basket.containsKey(s) ) {
				n = basket.basket.get(s);
			}
			basket.basket.put(s, ++n);
		}
		this.transactions.add(basket);
	}
	public void printTransactions() {
		int i = 0;
		for (Basket b:this.transactions) {
			System.out.print(++i + "\t");
			System.out.print(b + "\n");
		}
	}
	// compute frequent items
	// set s as support threshold
	public void frequentItems(int s) {
		// store frequent itemsets in this
		// keep basket indexes in an array
		ArrayList< TreeMap<String, ArrayList<Integer> > > sets = new ArrayList<>();
		// find singletons
		TreeMap<String, Integer> singletons = new TreeMap<>();
		// compute singletons and its support
		for (Basket b:this.transactions) {
			for (String k:b.basket.keySet()) {
				int n = 0;
				if (singletons.containsKey(k)) {
					n = singletons.get(k);
				}	
				singletons.put(k, ++n);
			}		
		}
		// remove singletons with support < s
		ArrayList<String> to_remove = new ArrayList<>();
		for (String k:singletons.keySet()) {
			if (singletons.get(k) < s)
				to_remove.add(k);				
		}
		for (String k:to_remove) {
			singletons.remove(k);
		}

		// next steps
		int step = 1;

		// keep support and confidence values for each set
		HashMap<String, Double> support = new HashMap<>();
		HashMap<String, Double> confidence = new HashMap<>();
		int N = this.transactions.size();

		// sets[0] = singletons
		TreeMap<String, ArrayList<Integer> > singleton_set = new TreeMap<>();
		for (String item:singletons.keySet()) {
			// find baskets which contain item
			ArrayList<Integer> baskets = new ArrayList<>();
			// baskets numbers starts from 1
			int index = 1;
			for (Basket b:this.transactions) {
				if (b.basket.containsKey(item))
					baskets.add(index);
				index++;
			}
			singleton_set.put(item, baskets);
		}
		sets.add(singleton_set);
		ArrayList<String> single_keys = new ArrayList<>( singletons.keySet() );
		//
		while( sets.get(step-1).size() > 0) {
			
			TreeMap<String, ArrayList<Integer> > set = new TreeMap<>();
			
			ArrayList<String> keys = new ArrayList<>( sets.get(step-1).keySet() );
						
			for (int i=0; i< keys.size(); i++) {
				for (int j=0; j<single_keys.size(); j++) {
					//
					int count = 0;
					// keep baskets index which contains items A+B
					ArrayList<Integer> baskets = new ArrayList<>();
					
					// keep items in a TreeSet to maintain sorting
					String[] A_set = keys.get(i).split("-");
					TreeSet<String> A_tree = new TreeSet<>();
					for(String A:A_set) {
						A_tree.add(A);
					}
					
					// item B
					String B = single_keys.get(j);
					// add item B to all items in A
					TreeSet<String> B_tree = new TreeSet<>(A_tree);
					B_tree.add(B);
					//
					// construct set key as X-Y-Z
					String set_key = "";
					int n = 0;
					for (String A:B_tree) {
						set_key += A;
						if(n++<B_tree.size()-1)
							set_key += "-";
					}

					//
					int count_A_tree_baskets = 0;
					if (!A_tree.contains(B) && !set.containsKey(set_key)) {
						// check in every basket for this set of items A_set	
						int index = 1;
						for (Basket b:this.transactions) {					
							int contains_set = 0;
							for(String A:A_set) {
								 if (b.basket.containsKey(A) )
									 contains_set++;
							}
							if(contains_set == A_set.length) {
								if(b.basket.containsKey(B)) {
									baskets.add(index);
									count++;
								}
							}
							index++;
						}	
						// what is support(X) ?
						String A_tree_key = "";
						int k = 0;
						for (String A:A_tree) {
							A_tree_key += A;
							if(k++<A_tree.size()-1)
								A_tree_key += "-";
						}				
						if (sets.get(step-1).containsKey(A_tree_key))
							count_A_tree_baskets = sets.get(step-1).get(A_tree_key).size();
					}
					// save only if items number exceeds threshold
					if (count >= s) {
						// pair	
						set.put(set_key, baskets);
						// support(X->Y) = support(X and Y) / N
						support.put(set_key, (double)count / N);
						// confidence(X->Y) =  support(X and Y) / support(X)
						if (count_A_tree_baskets != 0)
							confidence.put(set_key, (double)count / count_A_tree_baskets);
					}
				}
			}								
			sets.add(set);	

			//
			step++;
		}		
		this.itemsets = sets;
		this.support = support;
		this.confidence = confidence;
	}
	public void printItemsets() {
		for(TreeMap<String, ArrayList<Integer>> set:this.itemsets) {
			// print 
			if (set.size() > 0) {
				System.out.println();

				for (String k:set.keySet()) {
					System.out.print(k + " in " + set.get(k).size() + " baskets");
					if (this.support.get(k) != null)
						System.out.printf(", support: %.3f, ", this.support.get(k));
					if (this.confidence.get(k) != null)
						System.out.printf("confidence: %.3f", this.confidence.get(k) );
					System.out.print("\n");
				}
				
			}	
		}
	}
	public double confidence(ArrayList<String> A, ArrayList<String> B) {
		int deltaA = 0;
		int deltaAB = 0;
		for (Basket basket:this.transactions) {					
			int contains_set = 0;
			for(String a:A) {
				 if (basket.basket.containsKey(a) )
					 contains_set++;
			}
			if(contains_set == A.size()) {
				deltaA++;
				contains_set = 0;
				for(String b:B) {
					 if (basket.basket.containsKey(b) )
						 contains_set++;
				}				
				if(contains_set == B.size())
					deltaAB++;
			}
		}		
		return (double) deltaAB / deltaA;
	}	
	public void confidenceAll(int s, double treshold) {
		// get all items
		TreeMap<String, Integer> items = new TreeMap<>();
		for (Basket b:this.transactions) {
			for (String k:b.basket.keySet()) {
				int n = 0;
				if (items.containsKey(k))
					n = items.get(k);				
				items.put(k, ++n);
			}			
		}
		// remove singletons with support < s
		ArrayList<String> to_remove = new ArrayList<>();
		for (String k:items.keySet()) {
			if (items.get(k) < s)
				to_remove.add(k);				
		}
		for (String k:to_remove) {
			items.remove(k);
		}

		//
		ArrayList < HashMap<String, Double> > confidence = new ArrayList<>();
		//
		ArrayList<String> keys = new ArrayList<>(items.keySet());
		//
		HashMap<String, Double> map = new HashMap<>();
		for (String k:keys) {
			map.put(k, 0d);
		}
		confidence.add(map);
		
		int counter = 0;
		int steps = 4;
		int step = 0;
		while(step < steps) {
			
			HashMap<String, Double> confidence_map = new HashMap<>();
			
			for (String keyA:confidence.get(step).keySet()) {
				
				ArrayList<String> A = new ArrayList<>();
				for (String str:keyA.split("-")){
					A.add(str);
				}
				
				for (String keyB:keys) {
					
					if ( !A.contains(keyB)) {
	
						ArrayList<String> B = new ArrayList<>();
						B.add( keyB );
						//
						double d = this.confidence( A, B );
						//
						String set_key = "";
						int k = 0;
						ArrayList<String> AB = new ArrayList<>(A);
						AB.addAll(B);
						for (String a:AB) {
							set_key += a;
							if(k++ < AB.size()-1)
								set_key += "-";
						}		
						int countSet = this.countSet(AB);
						if ( countSet >= s) {
							confidence_map.put(set_key, d);
						}
					}
				}
			}
			confidence.add(confidence_map);
			for (String k:confidence_map.keySet()) {
				if ( confidence_map.get(k) >= treshold )
					System.out.println(++counter + " confidence " + k + " : " + confidence_map.get(k));
			}
			step++;
		}
	}
	private int countSet(ArrayList<String> Set) {
		int countSet=0;
		for (Basket b:transactions) {
			int count = 0;
			for (String s:Set) {
				if (b.basket.containsKey(s))
					count++;
			}
			if (count == Set.size())
				countSet++;
		}
		return countSet;
	}
	// find in which baskets items appear together
	// return baskets by indexes of transactions array
	public ArrayList<Integer> whichBaskets(ArrayList<String> items){
		ArrayList<Integer> result = new ArrayList<>();
		// baskets are numbered from 1
		int i=1;
		//look in each transaction
		for (Basket b:this.transactions) {
			int count = 0;
			for(String item:items) {
				if (b.basket.containsKey(item)) {
					count++;
				}
			}
			if (count == items.size())
				result.add(i);
			i++;
		}
		return result;
	}
	// print itemsets of level lv
	public void printSetsLevel(int lv) {
		TreeMap<String, ArrayList<Integer>> set = this.itemsets.get(lv);
			// print 
			System.out.println();
			if (set.size() > 0) {
				System.out.println("frequent items level " + (lv+1));

				for (String k:set.keySet()) {
					System.out.print(k + " in " + set.get(k) + " baskets");
					if (this.support.get(k) != null)
						System.out.printf(" support: %.3f, ", this.support.get(k));
					if (this.confidence.get(k) != null)
						System.out.printf("confidence: %.3f", this.confidence.get(k) );
					System.out.print("\n");
				}
				
			}	
		
	}
}
