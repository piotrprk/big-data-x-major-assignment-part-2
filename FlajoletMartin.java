import java.util.ArrayList;

class FlajoletMartin{
	ArrayList<Integer> stream;
	// hash function CONST
	// hash function is: h(x) = (ax + b) % c
	int a;
	int b;
	int c;
	// binary length
	int bin_length;
	
	public FlajoletMartin(int a, int b, int c, int len) {
		this.stream = new ArrayList<>();
		this.a = a;
		this.b = b;
		this.c = c;
		this.bin_length = len;
	}
	

	public void printStream() {
		System.out.println(this.stream);
	}
	
	// hash function concept
	// y = a*x + b mod c
	public int hx(int x, int a, int b, int c) {
		return (a*x + b) % c;
	}
	// take this class const for hash function
	public int h(int x) {
		return hx(x, this.a, this.b, this.c);
	}	
	// transform integer to binary with given length
	// len - binary chain length
	// returns binary chain of given length
	public String intToBinary(int x, int len) {
		// set binary length to 2^len > x
		if (Math.pow(2, len) < x)
			return "ERROR";
		StringBuilder binary = new StringBuilder();
       int shift = len - 1;
       while (shift >= 0) {
    	   // shift x and filter
    	   int bit = ( x >> shift ) & 1;
    	   // add result do binary chain
    	   binary.append(bit);
    	   shift--;
       }		
		return binary.toString();
	}	
	// for given binary length
	public String intToBinary(int x) {
		return this.intToBinary(x, this.bin_length);
	}
	// get tail length
	// count 0 at the end of string
	public int tailLength(String binary) {
		String[] chars = binary.split("(?!^)");
		int counter = 0;
		for (int i=chars.length-1; i >= 0; i--) {
			if (chars[i].equals("0") )
				counter++;
			else if (chars[i].equals("1") )
				break;
		}
		return counter;
	}
	// count tail length with String.lastIndexOf function
	public int tailLength2(String binary) {		
		int i = binary.lastIndexOf("1");		
		return binary.length() - i - 1;
	}
	// count max number of trailing 0 in binary string
	// return int with max trailing zeros when converted to binary
	// input are integers from-to
	public int maxT(int from, int to) {
		int max = 0;
		int max_i = 0;
		for (int i=from; i<=to; i++) {
			int hashed = this.h(i);
			String bin = this.intToBinary(hashed, this.bin_length);
			int T = this.tailLength2(bin);
			if (T > max) {
				max = T;
				max_i = i;
			}
		}		
		return max_i;
	}

	// find max tail length T and print
	// input a stream of integer numbers
	public int maxTandPrint(int[] stream) {
		System.out.println("Flajolet-Martin with hash function h(x) = ("+this.a+"*x"+" + "+this.b+") % "+this.c);
		System.out.println();
		int max_T = 0;
		for (int x:stream) {
			// hash input
			int hashed = this.h(x);
			// convert to binary
			String binary = this.intToBinary(hashed);
			System.out.println("input: " + x + " -> hashed to: " + hashed + " -> binary: " + binary);
			// determine tail length
			int T = this.tailLength2(binary);
			if (T > max_T) {
				max_T = T;
			}
			System.out.println("tail length T: " + T + " estimated no of distinct elements 2^T: " + ((int)Math.pow(2, T)));
			System.out.println();
		}
		return max_T;
	}
}